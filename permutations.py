#!/usr/bin/env python

# Copyright 2024 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# separate the existing items from the total list to produce the remainder.
def separate(total, existing):
    remainder = []
    if existing:
        for item in total:
            if item not in existing:
                remainder.append(item)
    else:
        remainder += total
    return remainder

# Add permutations for all partial permutation of the values starting with existing list.
def partial_permutations(values, existing, output):
    remainder = separate(values, existing)
    if len(remainder) > 2:
        for item in remainder:
            partial_permutations(values, existing + [item], output)
    else:
        output += [existing + [remainder[0], remainder[1]]]
        output += [existing + [remainder[1], remainder[0]]]

# Get a list of all the permutations for a list of numbers.
def permutations(values):
    output = []
    for value in values:
        partial_permutations(values, [value], output)
    return output
