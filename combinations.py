#!/usr/bin/env python

# Copyright 2024 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Return all the combinations of three for the list.
def triplet_combos(l):
    triplets = []
    for p1 in range(0, len(l)):
        for p2 in range(p1 + 1, len(l)):
            for p3 in range(p2 + 1, len(l)):
                triplet = [l[p1], l[p2], l[p3]]
                triplets += [triplet]
    return triplets

# Return all the combinations for the input list, including an empty item.
def combinations_all(l):
    combs = [[]]
    for element in l:
        for sub_set in combs.copy():
            new_sub_set = sub_set + [element]
            combs.append(new_sub_set)
    return combs

# Python example of getting all the combinations, without an empty item.
def combinations(l):
    combs = []
    for element in l:
        if not combs:
            combs.append([element])
        else:
            combs_copy = combs.copy()
            combs.append([element])
            for sub_set in combs_copy:
                new_sub_set = sub_set + [element]
                combs.append(new_sub_set)
    return combs
