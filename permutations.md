# Example of permutations for a set of 3 and 4 numbers.

```
Example of permutations for 1, 2, and 3
1,2,3
1,3,2
2,1,3
2,3,1
3,1,2
3,2,1

Example of permutations for 1, 2, 3, and 4
1,2,3,4
1,2,4,3
1,3,2,4
1,3,4,2
1,4,2,3
1,4,3,2
2,1,3,4
2,1,4,3
2,3,1,4
2,3,4,1
2,4,1,3
2,4,3,1
3,1,2,4
3,1,4,2
3,2,1,4
3,2,4,1
3,4,1,2
3,4,2,1
4,1,2,3
4,1,3,2
4,2,1,3
4,2,3,1
4,3,1,2
4,3,2,1
